<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Company::class, 20)->create()->each(function ($company) {
            $count = rand(1, 4);
            for($i = 0; $i < $count; $i++) {
                $company->contacts()->save(factory(App\Models\Contact::class)->make());
            }
        });
    }
}
