<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['company_id', 'firstname', 'name'];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
