<?php

return [
    'home'         => 'Home',
    'orders'       => 'Orders',
    'contact'      => 'Contacteer ons',
    'profile'      => 'Profiel',
    'firstname'    => 'Voornaam',
    'name'         => 'Naam',
    'company'      => 'Bedrijf',
    'actions'      => 'Acties',
    'add_contact'  => 'Contact toevoegen',
    'edit_contact' => 'Contact wijzigen'
];