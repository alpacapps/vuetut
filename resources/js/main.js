import Vue from 'vue'
import Vuex from 'vuex';
import App from './App'
import store from './store/store.js'
import router from './router'

import vuexI18n from 'vuex-i18n';
import Locales from './vue-i18n-locales.generated.js';

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

const langStore = new Vuex.Store();

Vue.use(vuexI18n.plugin, langStore);

Vue.i18n.add('en', Locales.en);
Vue.i18n.add('nl', Locales.nl);

// set the start locale to use
Vue.i18n.set("nl");

Vue.use(require('vue-moment'));

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/nl'

Vue.use(ElementUI, {locale});

const app = new Vue(
    {
        el:         '#app',
        store,
        router,
        template:   '<App/>',
        components: {
            App
        }
    }
);
