import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

// Containers
import Full from '../layout/Master'

import PageHome from '../pages/PageHome'
import PageOrders from '../pages/PageOrders'
import PageContact from '../pages/PageContact'
import PageProfile from '../pages/PageProfile'

import PageDashboard from '../pages/profile/PageDashboard'
import PageDocuments from '../pages/profile/PageDocuments'
import PageSettings from '../pages/profile/PageSettings'

export default new Router({
                              mode:            'history',
                              linkActiveClass: 'open active',
                              routes:          [
                                  {
                                      path:      '/',
                                      component: Full,
                                      children:  [
                                          {
                                              path:      '/',
                                              name:      'home.index',
                                              component: PageHome
                                          },
                                          {
                                              path:      '/orders',
                                              name:      'orders.index',
                                              component: PageOrders
                                          },
                                          {
                                              path:      '/contact',
                                              name:      'contact.index',
                                              component: PageContact
                                          },
                                          {
                                              path:      '/profile',
                                              name:      'profile.index',
                                              component: PageProfile,
                                              children:  [
                                                  {
                                                      path: '/profile/dashboard',
                                                      name: 'profile.dashboard',
                                                      component: PageDashboard
                                                  },
                                                  {
                                                      path: '/profile/documents',
                                                      name: 'profile.documents',
                                                      component: PageDocuments
                                                  },
                                                  {
                                                      path: '/profile/settings',
                                                      name: 'profile.settings',
                                                      component: PageSettings
                                                  },
                                              ]
                                          }
                                      ]
                                  }
                              ]
                          });