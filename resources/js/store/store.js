import Vue from 'vue';
import Vuex from 'vuex';
import mutations from './mutations';
import actions from './actions';

import ContactsModule from './modules/contactsModule'

Vue.use(Vuex);

//=======vuex store start===========
const store = new Vuex.Store(
    {
        strict:    true,
        modules:   {
            ContactsModule
        },
        state:     {
            site_name: "Vuetuts",
        },
        actions:   actions,
        mutations: mutations
    });

//=======vuex store end===========
export default store