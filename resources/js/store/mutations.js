let mutations = {
    ROUTE_CHANGE(state, loader) {
        if (loader == "start") {
            state.preloader = true;
        }
    },
    CHANGE_PAGE_TITLE(state, title) {
        state.page_title = title
        document.title = title + " - " + state.site_name
    }
};

export default mutations