const actions = {
    getContacts({commit, state}, params) {
        return new Promise((resolve, reject) => {
            var queryParams = {...params, ...state.filters};

            axios.get('/api/contacts', {params: queryParams}).then((response) => {
                commit('CONTACTS_FETCHED', response.data);
                resolve();
            }, (error) => {
                reject(error.response);
            });
        });
    },
    getDependencies({commit}, data) {
        return new Promise((resolve, reject) => {
            axios.get('/api/contacts/dependencies').then((response) => {
                resolve(response.data.companies);
            }, (error) => {
                reject(error.response);
            });
        });
    },
    saveContact({commit}, contact) {
        return new Promise((resolve, reject) => {
            if(contact.id) {
                var client = {
                    'method' : 'put',
                    'url': 'api/contacts/' + contact.id
                };
            } else {
                var client = {
                    'method' : 'post',
                    'url': 'api/contacts'
                };
            }

            axios[client.method](client.url, contact).then((response) => {
                resolve();
            }, (error) => {
                reject(error.response);
            });
        });
    },
    deleteContact({commit}, contact) {
        return new Promise((resolve, reject) => {
            axios.delete('/api/contacts/' + contact.id).then((response) => {
                resolve();
            }, (error) => {
                reject(error.response);
            });
        });
    }
};

export default actions;